/*                                                             parametrage et demarage                                                           */
/*                     demmarer le serveur                              */

    -- demarer un serveur SQL dans le terminal
    -- aller dans le repertoir mysql/bin
        ./ mysqld.exe -- cosole

    -- ce connecter sur l'utilisateur root au serveur dans le terminal
    -- dans le repertoir mysql
        ./ mysql.exe -u root -p

/*                     configurer le serveur                            */

    --

/*                                                             commande SQL                                                                      */
/*                      commande des databases                          */

    -- créé une database
        CREATE DATABASE `nom_database`;
        CREATE DATABASE `nom_database` IF NOT EXISTS;

    -- utiliser une database
        USE ``nom_databases`;

    -- supprimer une database
        DROP DATABASE nom_dataabase;
        DROP DATABASE nom_dataabase IF NOT EXISTS;

    -- afficher les databases qui existe sur le serveur
        SHOW `nom_databases`;

    -- importer et executer un fichier SQL
        SOURCE chemin/du/fichier.sql 



/*                      commande des tables                             */

    -- afficher les tables d'une database
        SHOW TABLES FROM `nom_databases`;
        SHOW TABLES;


    CREATE TABLE 
        -- créé une base de donner
            CREATE TABLE `nom_table`(   `id_user` INT, 
                                        `user_name` VARCHAR(65),
                                        `user_birthday` DATE);

            CREATE TABLE `nom_table` IF NOT EXISTS( `id_user` INT(), 
                                                    `user_name` VARCHAR(65),
                                                    `user_birthday` DATE);


    DESCRIBE
        -- decrire une table et c'est specificiter
            DESCRIBE `nom_table`;

    DROP
        -- supprimer une table
            DROP TABLE `nom_table`;
            DROP TABLE `nom_table` IF EXISTS;



/*                      modifier une table                              */
    ALTER TABLE `nom_table` ... ;

        ADD
            -- ajouter une colone
                ALTER TABLE `nom_table` ADD `nom_nouvelle_colone` VARCHAR(10)

        DROP
            -- supprimer une colone
                ALTER TABLE `nom_table` DROP `nom_colone`;

        CHANGE
            -- changer le nom d'une colone
                ALTER TABLE `nom_table` CHANGE `ancien_nom_colone` `nouveau_nom_colone`;

        MODIFY
            -- modifier le type de donner d'une colone
                ALTER TABLE `nom_table` MODIFY `nom_colone` VARCHAR(20);



/*                      les contraintes                                 */

    -- les contraintes sont un ensembler de regle 
    -- que l'ont vas definir sur les colone.
    -- c'est regle vont servir a ameliorer la performance
    -- et a la coherence de la table

    -- elle sont definie lors de la crééation de la table 
    -- mais peut aussi etre alterer 


    NOT NULL
        -- la valeur doit peut etre laisser null

    DEFAULT ..
        -- la valeurs apres le default est entrer part defaut si aucune 
        -- valeur n'est donner quand une nouvelle ligne est ajouter a la table

    CHECK (`nom_colonne` condition valeur)
    CHECK (`nom_colonne` < 5)
        -- permet de mettre une condition a respecter lors de l'ajout d'une nouvelle ligne

    UNIQUE (`nom_colone`)
        -- chaque champs dans cette colone sera unique
        -- si ont essaye dit entrer un champ deja present dans cette colone, renvera une erreur

    PRIMARY KEY (`nom_colone`)
        -- la colone sera la clef primaire de la table
        -- sert au referencement et a la performance des requete

    AUTO_INCREMENT
        -- la colone s'incremantera automatiquement
        -- sert surtout pour la clef primaire

    FOREIGN KEY (`nom_colone`) REFERENCES (`nom_autre_table`)
        -- la colone contiendra les clef d'une autre autre table
        -- sert au jointure entre table
        -- le TYPE doit etre le meme que celui de la clef

    CREATE INDEX `nom_index` ON `nom_table`(`nom_colone`);
        -- créé un index qui augmente la vitesse d'execution des requete


    -- exemple

        CREATE TABLE `nouvelle_table`(  `id_user` INT NOT NULL AUTO_INCREMENT,
                                        `user_name` VARCHAR(10) NOT NULL,
                                        `user_age` INT NOT NULL,
                                        CHECK( `user_age` < 110),
                                        PRIMARY KEY (`id_user`));


/*                      les type de donner                              */



    -- les types nmeriques

        /* -----------------------------------------------------------------------------------------------------------------
        |      TYPE      |   TAILLE  |               LIMITE(signé)              |             LIMITE(non signé)             |
        |                |           |                                          |                                           |
        | bit(m)         |  1 octet  |  b'0000000' a b'111111                   |  b'000000' à b'111111'                    |
        | tinyint(m)     |  1 octet  |  -128 a 127                              |  0 à 255                                  |
        | bool/boolean   |           |                                          |                                           |
        | smallint(m)    |  2 octet  |  -32768 a 32767                          |  0 à 65535                                |
        | mediumint(m)   |  3 octet  |  -8388608 à 8388607                      |  0 à 16777215                             |
        | int(m)         |  4 octet  |  -2147483648 à 2147483647                |  0 à 4294967295                           |
        | bigint(m)      |  8 octet  |  -2^63 à 2^63-1                          |  0 à 2^64-1                               |
        | decimal(m/d)   |  8 octet  |  jusqu'à 65 chiffres                     |  jusqu'à 65 chiffres                      |
        | float(m/d)     |  4 octet  |  -1,175494351.10^-38 à 3,402823466.10^38 |  1,175494351.10^-38 à 3,402823466.10^38   |
         ----------------------------------------------------------------------------------------------------------------- */




    -- les type de chaines de caractere

        /*-----------------------------------------------------------------------
        |    TYPE     |        TAILLE      |          LIMITE                     |
        |             |                    |                                     |
        | char(m)     | m octet            |   255 caractères / octet            |
        | varchar(m)  | m + 1 octet        |   255 caractères / octet            |
        | tinytext    | m + 1 octet        |   255 caractères / octet            |
        | text(m)     | m + 2 octet        |   65535 caractères / octet          |
        | mediumtext  | m + 3 octet        |   16777215 caractères / octet       |
        | longtext    | m + 4 octet        |   4294967295 caractères / octet     |
        | set         | 1.2.3.4 ou 8 octets|   8, 16, 24, 32 ou 64 choix         |
        | enum        | 1 ou 2 octet       |   255 ou 65535 choix                |
         -----------------------------------------------------------------------*/



    -- types pour les dates

        /*-----------------------------------------------------------------------------
        |     TYPE     |     TAILLE      |                LIMITE                       |
        |              |                 |                                             |
        |  date        |   3 octets      | 1000-01-01 à 9999-12-31                     |
        |  time        |   3 octets      | 00:00:00 à 23:59:59                         |
        |  datetime    |   8 octets      | 1000-01-01 00:00:00 à 9999-12-31 23:59:59   |
        |  year        |   1 octets      | 1901 à 2155                                 |
        |  timestamp   |   4 octets      | 19700101000000 à 20380119031407             |
         -----------------------------------------------------------------------------*/



/*                      lecture de donner                               */


    SELECT
        -- pour selectionner des donner ont utilise la commande SELECT

        SELECT * FROM `nom_table`;
            -- selectionne toute les colone (*) de `nom_table`

        SELECT DISTINCT `nom_colone` FROM `nom_table`;
            -- selectionne toute les valeur distinc (enleve les doublon) de `nom_colone` dans la table `nom_table`

        AS
            -- permet de renomer a la sorti le nom d'une ou des colone
            -- cela ne les modifie pas dans la database
                SELECT `nom_colone` AS `nouveau nom colone` FROM `nom_table`;




        -- filter les requete

        WHERE
            -- sert a mettre un filtre sur les donner via des operateur
            /*          =
                        !=
                        <
                        >
                        <=
                        >=
            */
            -- ont peut aussi utiliser AND, OR pour avoir plusieur condition et NOT
                SELECT * FROM `nom_table` WHERE `nom_colone_1` = 15 AND `nom_colone_2` != 4 OR NOT `non_colone_3` > 90;

            IN / NOT IN (liste de nombre)
                -- permet de definir un liste de valeur
                    SELECT * FROM `nom_table` WHERE `nom_colone` in (2,8,160) or `nom_colone_2` NOT IN(12,15,19);

            BETWEEN 'nombre' AND 'nombre'
                -- definie une plage de valeur
                    SELECT * FROM `nom_table` WHERE `nom_colone` BETWEEN 0 and 20;
                    -- si nom_colone est comprit entre 0 et 20


            LIKE "chaine de caractere"
                -- permet de compater des chaine de caractere

                    SELECT * FROM `nom_table` WHERE `nom_colone` LIKE "info";
                        -- si nom_colone = info

                    SELECT * FROM `nom_table` WHERE `nom_colone` LIKE "info%";
                        -- si nom_colone commence part info

                    SELECT * FROM `nom_table` WHERE `nom_colone` LIKE "%info";
                        -- si nom_colone termine part info

                    SELECT * FROM `nom_table` WHERE `nom_colone` LIKE "%info%";
                        -- si nom_colone contient info

                    SELECT * FROM `nom_table` WHERE `nom_colone` NOT LIKE "%info%";
                        -- si nom_colone ne contient pas info


            `nom_colone` IS NULL / IS NOT NULL
                -- si le champ est null / si le champ n'est pas null
                    SELECT * FROM `nom_table` WHERE `nom_colone` IS NOT NULL OR `nom_colone_2` IS NULL;


            ORDER BY `nom_colone`
                -- permet de classer les donner part ordre alphabetique ou croissant
                -- ne modifie pas les donné dans la database, uniquement dans le retour de la requete

            ORDER BY `nom_colone` DESC
                -- permet de classer les donner part ordre ALPHABETIQUE INVERSER ou DECROISSANT
                -- ne modifie pas les donné dans la database, uniquement dans le retour de la requete

            LIMIT 'nombre'
                -- limite le nombre de reponse de la requete au 'nombre'



/*                      enregistrer des donner                          */
    INSERT INTO
        -- insert de nouvelle donner dans la table

            INSERT INTO `nom_table`(`nom_colone_1`,`nom_colone_2`,`nom_colone_3`)
            VALUES('donner 1', 1444, 'AAA-MM-JJ');
                -- cette requete permet de gerer dans qu'elle champs ont insert qu'elle donner
                -- les parenthese apres la table donne le nom des colone ou inserer les donner et leurs ordre
                -- VALUES donne les donner a inserer et suis l'ordre etablit avant
            
            INSERT INTO `nom_table`(`nom_colone_1`,`nom_colone_2`,`nom_colone_3`)
            VALUES('donner 1', 1444, 'AAA-MM-JJ'),
            ('donner 2', 155, 'AAA-MM-JJ'),
            ('donner 3', 166666, 'AAA-MM-JJ');
                -- ont peut entrer plusieurs ligne en meme temps 



/*                      modifier et supprimer des donner                */
    UPDATE `nom_table`
        -- permet de modifier des donner

            SET `nom_colone` = 'nouvelle valeur'
                -- selectionne le ou les champ a modifier et lui atribut ca nouvelle valeur

            WHERE `nom_colone` condition valeur
                -- definie la/les ligne a modifier

                    LIKE
                        -- pour filter dans les chaine de caractere


            -- exemple

                UPDATE `nom_table` SET `nom_colone` = 'nouvelle valeur';
                    -- toutes les valeur de la colone `nom_colone` de la table `nom_table` sont passer a 'nouvelle valeur'

                UPDATE `nom_table` SET `nom_colone` = 'nouvelle valeur' WHERE `colone_de_reference` LIKE "reference" ;
                    -- les donner de la colone `nom_colone` de la table `nom_table` dont `colone_de_reference` est egale a 'reference' sont modifier a 'nouvelle valeur'

                UPDATE `nom_table` SET `nom_colone_1` = 'nouvelle valeur 1',`nom_colone_2` = 'nouvelle valeur 2' WHERE `colone_de_reference` = 'reference'
                    -- modifie deux champs des ligne dont `colone_de_reference` est egale a reference


    DELETE FROM `nom_table`
        -- permet de suppimer des donner

            WHERE `nom_colone` condition valeur
                -- definie la/les ligne a suprimer

                    LIKE
                        -- pour filter dans les chaine de caractere

            -- exemple
                DELETE FROM `nom_table` WHERE `nom_colone` = 'valeur';
                    -- supprime tous les lignes dont la valeurs de la colone `nom_colone` est egale a 'valeur'

                DELETE FROM `nom_table` WHERE `nom_colone` LIKE "%reference%" AND `nom_colone_2` < 50;
                    -- supprime tous les lignes dont la valeurs de la colone `nom_colone` est contient 'reference' et dont la colone `nom_colone_2` est inferieur a 50

                DELETE FROM `nom_table`;
                    -- supprime tous les enregistrement d'une table

    TRUNCATE FROM `nom_table`;
        -- supprime tous les enregistrement d'une table, 
        -- est plus efficace que DELETE FROM `nom_table`;




/*                      fonction d'agregation                           */

    -- les fonction d'agrega sont des fonction qui servent principalement a faire des statistique sur les table

    -- forme de la requete

        SELECT NOM_FONTION(`colone_a_appliquer`) FROM `nom_table`;
            -- appliquent la fonction NOM_FONCTION a la colone `colone_a_appliquer` de la la table `nom_table`

        SELECT NOM_FONTION(`colone_a_appliquer`) AS `nouveau nom` FROM `nom_table`;
            -- appliquent la fonction NOM_FONCTION a la colone `colone_a_appliquer` de la la table `nom_table` et affiche les resultat sous le nom `nouveau nom`

    -- fonction

        COUNT(*)
            -- count sert a compter le nombre de ligne contenue sur une table

                SELECT COUNT(*) FROM `nom_table`;
                    -- retourne le nombre de ligne de la table `nom_table`

        AVG(`nom_colone`)
            -- calcul la moyenne des valeurs contenue dans la colone `nom_colone`

                SELECT AVG(`nom_colone`) AS `moyenne` FROM `nom_table`;
                    -- calcule la moynne de `nom_colone` de la table `nom_table` et retoune le resulta sous le nom `moyenne`


        MAX(`nom_colone`)
            -- retourne la valeurs maximum de la colone
            -- si appliquer sur une chaine de caractere, 
            -- retourne celle commencent part la lettre la plus proche de la fin de l'alphabet

                SELECT MAX(`nom_colone`) FROM `nom_table`;
                    -- retourne la valeurs maximum de la colone `nom_colone` de la table `nom_table`

        MIN(`nom_colone`)
            -- retourne la valeurs maximum de la colone
            -- si appliquer sur une chaine de caractere, 
            -- retourne celle commencent par la lettre la plus proche du debut de l'alphabet

                SELECT MIN(`nom_colone`) FROM `nom_table`;
                    -- retourne la valeurs minimum de la colone `nom_colone` de la table `nom_table`

        SUM(`nom_colone`)
            -- retourne la somme des nombre contenue dans la colone
            
                SELECT SUM(`nom_colone`) FROM `nom_table`;
                    -- retourne la somme des element de `nom_colone` de la table `nom_table`

                SELECT SUM(`nom_colone`) AS `somme des element` FROM `nom_table`;
                    -- retourne la somme des element de `nom_colone` de la table `nom_table` sous le nom somme des element



/*                      grouper les resultat                            */

    -- le groupement via groupe by permet de filtrer des fonction sur un parametre
    -- part exemple si ont a une colone 'pays' et une autres 'personnes', ont peut retourne le nombre de personne part pays

    GROUP BY `nom_colone_filtre`
        -- definie la colone qui servira de filtre

            --exemple:

                SELECT COUNT(*) 
                FROM `nom_table` 
                GROUP BY `nom_colone_filtre`;
                    -- renvoie un tableau du nombre de ligne pour chaque filtre `nom_colone_filtre` dans la table `nom_table`
                    -- ATTENTION, ce modele ne renvoie que la colone du total, sans dire a quoi elle corespond

                SELECT COUNT(*),`nom_colone_filtre` 
                FROM `nom_table` 
                GROUP BY `nom_colone_filtre`;
                    -- renvoie un tableau du nombre de ligne pour chaque filtre `nom_colone_filtre` dans la table `nom_table`
                    -- et la colone qui sert de filtre, ce qui nous donne un total pour chaque champs

                SELECT COUNT(*) AS `nombre de ligne part filtre`,`nom_colone_filtre` 
                FROM `nom_table` 
                GROUP BY `nom_colone_filtre`;
                    -- renvoie un tableau du nombre de ligne pour chaque filtre `nom_colone_filtre` dans la table `nom_table` renomer en `nombre de le ligne pour chaque champs`
                    -- et la colone qui sert de filtre, ce qui nous donne un total pour chaque champs

    
            WITH ROLLUP;
                -- permet d'ajouter un ligne pour le global,
                -- si ont fait la sum des element grouper part une colone, WITH ROLLUP rajoutera un ligne pour la sum global

                    -- exemple

                        SELECT SUM(`nom_colone`),`nom_colone_filtre` 
                        FROM `nom_table` 
                        GROUP BY `nom_colone_filtre` WITH ROLLUP;
                            -- revoie la somme de `nom_colone` filtrer part `nom_colone_filtre` et termine part la somme total de `nom_colone`

            HAVING 'condition'
                -- sert a filtrer retour du GROUP BY part une condition

                --exemple:

                    SELECT SUM(`nom_colone`), `nom_colone_filtre`
                    FROM `nom_table`
                    GROUP BY `nom_colone_filtre` HAVING SUM(`nom_colone`) > 30;
                        -- revoie les somme de `nom colone` en fonction du filtre `nom_colone_filtre` et sous condition qu'il soit supperieur a 30



/*                      unions et intersection                          */

    UNION
        -- permet de concatenner deux table dans une recherche
        -- prend en compte les donblon

        --les 3 regles pour pouvoir faire un UNION
            -- le meme nombre de colone 
            -- le meme type de donner
            -- le meme ordre pour les colone des deux table a concatener

        --exemple:
            SELECT * FROM `nom_table_1`
            UNION
            SELECT * FROM `nom_table_2`;
                -- concat 2 table

            SELECT `nom_colone_1` FROM `nom_table_1`
            UNION 
            SELECT `nom_colone_2` FROM `nom_table_2`;
                -- concat juste 2 colone de deux table


    UNION ALL
    -- pareil que UNION mais ne prend pas en comtpe les doublon





/*                      sous-requetes                                   */

    -- les sous requete permette d'executer une requete dans une requete
    -- cela permet surtout d'aller chercher

    -- la requete imbriquer s'execute en premier
    

    -- exemple

        SELECT * FROM `nom_table_1`
        WHERE  `nom_colone_1` = ( 
                                SELECT * FROM `nom_table_2`
                                WHERE `nom_colone_2` = 'une valeurs' 
                                LIMIT 1
                                );
            -- ici, la requete depend du resultat de la sous requete, ca permet d'avoir des information sur plusieurs table et des les faire interagir
            -- elle ne doit retourner que 1 donner


        SELECT * FROM `nom_table_1`
        WHERE  `nom_colone_1` IN ( 
                                SELECT * FROM `nom_table_2`
                                WHERE `nom_colone_2` = 'une valeurs' 
                                );

            -- grace au IN, la sous-requete peut contenir plusieur object

        EXISTS
            -- la premiere requete est executer si la sous requete est valider

                SELECT * FROM `nom_table`
                WHERE EXISTS ( 
                                SELECT * FROM `nom_table_2`
                                WHERE `nom_colone` LIKE "%contient%"
                                ):
                    -- si la sous requete est valider alors la requete s'execute
    
    


/*                      les jointures                                   */

    -- les jointure sont les relation entre les table
    -- elle sont definit part le clef etrangere

    -- les tables qui utilise des clef etrangere doivent utiliser le moteur de gestion INOdb

        INNER JOIN `table_jointure` ON table_base.colone = table_jointure.colone_id_jointure;
            -- l'argument qui permet de faire une jointure entre les table


            -- exemple
                SELECT `colone_1`, `colone_2`, `colone_3`
                FROM `table_base`
                INNER JOIN `table_jointure` ON table_base.colone = table_jointure.colone_id_jointure;
                    -- revoie les donner avec la jointure, l'id est donc remplacr part les donner vers lequelle il pointe

        NATURAL JOIN;

            -- permet de faire des jointure dans le cas ou les donner sont du meme type (la clef et ce vers quoi elle pointe)
            -- les nom de colone doivent aussi etre identique

            -- exemple
                SELECT * 
                FROM `nom_table`
                NATURAL JOIN `nom_table_jointure` ON nom_table.colone = nom_table_jointure.colone;

        LEFT JOIN;
            -- la jointure gauche permet de recuperer tout ce qui est dans la premiere table meme si cela na pas d'equivalent dans la 2nd table



/*                      fonction chaines de caracteres                  */

    --

    CHAR_LENGTH(`nom_colone`)
        -- retourne le nombre de caractere (equivalent de len())

        -- exemple
            SELECT `nom_colone`,CHAR_LENGTH(`nom_colone`) FROM `nom_table`;
                -- retourne 2 colone, l'une avec les chaine de caractere et l'autre avec le nombre de caractere de c'est chaine


    CONCAT(`nom_colone_1`, ' - ',`nom_colone_2`)
        -- permet de concatener des donner, peut aussi prendre des chaine en dur en argument, et autant d'argument que nessessaire

        -- exemple
            SELECT CONCAT(`colone_1`,`colone_2`) FROM `nom_table`;
                -- retourne la concatecanation de la colone 1 et 2 pour chaque ligne de la table

            SELECT CONCAT(`colone_1`,' (',`colone_2`,')') FROM `nom_table`;
                -- retourne la concatenation colone_1 (colone 2) pour chaque ligne de la table `nom_table`

    UPPER(`nom_colone`)
        -- converti tout les caractere en majuscule

    LOWER(`nom_colone`)
        -- converti tout les caractere en minuscule

    REVERSE(`nom_colone`)
        -- inverse l'ordre des lettre de la chaine de caractere

    TRIM(`nom_colone`)
        -- supprime les espace blanc( espace ) avant et apres la chaine de caractere

    REPLACE(`nom_colone`,'nom a modifier', 'nouveau nom')
        -- permet de remplacer des donner dans une chaine de caractere
        -- peut s'apliquer a la chaine entiere comme une partie de celle ci

        --exemple
            UPDATE `nom_table` 
            SET `nom_colone` = REPLACE(`nom_colone`, 'ancien nom', 'nouveau nom')
            WHERE `nom_colone_2` LIKE '%donner%';
                -- remplace 'ancien nom' part 'nouveau nom' dans `nom_colone` si `nom_colone_2` contient 'donner'

                -- UPDATE permet de mettre a jours des donner dans une table

    SUBSTRING(`nom_colone`, 'chiffre')
        -- permet de faire des sous chaine de caractere
        -- il retournera la chaine de caractere mais elle commencera a 'chiffre' caractere

        -- exemple
            SUBSTRING("machainedecaractere",4) --> "hainedecaractere"

        SUBSTRIN(`nom_colone`,'chiffre','chifre2')
            -- ont peut passer un 3eme arguments qui determinera la longueur recuperer

            SUBSTRING("machainedecaractere",3,6)  --> "chaine"



/*                      fonction date et heure                          */

    -- pour formater les donner

    /*--------------------------------------------------------------------------
    |   code   |   exemple    |           represente            |     plage     |
    |          |              |                                 |               |
    |    %d    |    %d 01     |  definie le numero du jours     |   de 1 a 31   |
    |    %m    |    %m 05     |  definie le numero du mois      |   de 1 a 12   |
    |    %Y    |    %Y 2020   |  definie le numero de l'année   |               | 
    |          |              |                                 |               |
    |    %H    |    %H 15     |  definie le numero de l'heure   |   de 0 a 24   |
    |    %i    |    %i 30     |  definie le numeros de la minute|   de 0 a 59   |
    |    %S    |    %S 15     |  definie le numeros de la second|   de 0 a 59   |
     --------------------------------------------------------------------------*/

        --> 2020-05-01 15-30-15


    NOW()
        -- retourne la date actuel  --> YYYY-MM-DD HH-MM-SS

        -- exemple
            INSERT INTO `nom_table` VALUES ('1 info','2 info', NOW());
                -- insert dans la table nom_table 2 string et en 3eme valeurs la date et l'heure actuel


    -- recuperer un element d'une date
        DAY()
            -- permet de seletionner le jours dans une date

            -- exemple
                SELECT DAY(`nom_colone`) FROM `nom_table`;
                    -- retourne le numero du jours de la date cible

                SELECT DAY('2020-05-28 15-30-00');
                    -- retourne le numero du jours de la date donner en argument

        YEAR()
            -- permet de seletionner le jours dans une date

        MONTH()
            -- permet de seletionner le mois dans une date

        HOUR()
            -- permet de seletionner les heure dans une date

        MINUTE()
            -- permet de seletionner les minutes dans une date
        
        SECOND()
            -- permet de seletionner les secondes dans une date

    
    DATEDIFF('date','date')
        -- fait la difference entre deux date

        -- exemple
            SELECT DATEDIFF(`nom_colone_1`,`nom_colone_2`);
                -- retourne la difference en jours entre les date contenu dans les deux colone

            SELECT DATEDIFF("2020-02-17","2019-11-16");  
                --> retourne 93, le nombre de jours de difference

        


            









/*                      fonction mathematique                           */

    --


/*                      fonction de securite                            */

    --

